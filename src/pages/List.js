import React, { useState, useEffect } from 'react';
import { SafeAreaView, Image, StyleSheet, AsyncStorage } from 'react-native';

import SpotList from '../components/SpotList';

import logo from '../assets/logo.png'

export default function List() {

    const [techs, setTechs] = new useState([]);

    useEffect(() => {

    }, []);

    return (
        <SafeAreaView style={styles.container}>
            <Image style={styles.logo} source={logo} />

            {techs.map(tech => (
                <SpotList key={tech} tech={tech} />
            ))}    

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        height: 32,
        resizeMode: "contain",
        alignSelf: "center",
        marginTop: 10,
    },
});